# app de Notas de texto.

Implementar una API que permita publicar notas privadas de texto con imagenes y categorizarlas.

## Anónimo:

- Login: usando email + contraseña
- Registro: pide email + contraseña

## Usuarios Registrados:

- Ver su listado de notas (en el listado sólo se ven los títulos)
- Visualizar una nota
- Crear una nota: título, texto y categoría única (las categorías son fijas, no se pueden
  editar).
- Modificar sus notas: título, texto y categoría
- Eliminar una nota

## Funcionalidades extras de NOTA

- Marcar una nota como pública:
  Por defecto todas las notas son privadas y solo puede verlas el usuario que las
  creó, pero sí una nota se marca como pública esta se puede ver por cualquier
  usuario esté registrado y logueado en la aplicación o no.
  Las notas públicas sólo se puede acceder si se conoce la URL.
- Imagen: Se puede asociar una imagen (única) a cada nota.

# INSTALAR

- Instalar las dependencias necesarias mediante el comando `npm i`o `npm install`.

- Crear un fichero `.env` con los campos que figuran en `.env.example` y ajustar los valores.

- Ejecutar `npm run initDb` para crear las tablas necesarias.

- Ejecutar `npm run dev`o `npm start` para lanzar el servidor.

# ENTIDADES

## Usuario:

- idUsuario
- nombre
- email
- contraseña
- fechaCreacion
- fechaModificacion

## Categoria:

- idCategoria
- Titulo

## Notes:

- idNote
- userId
- idCategory
- image
- title
- text
- isPublic

# ENDPOINTS

## Usuarios:

- POST [/user] - Registro de usuario.

- POST [user/login] - Login de usuario (devuelve token).

- GET [/user/:id] - Obtiene la información del usuario.

## Categorias:

- GET [/category/:id] - Permite visualizar una nota categoría. TOKEN

- POST [/category] - Permite crear una categoria al usuario registrado. TOKEN

- PATCH [/category/:id] - Permite modificar una categoria al usuario registrado. TOKEN

- DELETE [/category/:id] - Borra una categoria solo si eres quien la creó. TOKEN

## Notes:

- GET [/notes] - Lista todas las notas del usuario registrado. TOKEN

- GET [/note/:id] - Permite visualizar una nota en concreto, no requiere TOKEN si la nota es publica

- POST [/note] - Permite crear una nota al usuario registrado. TOKEN

- PATCH [/note/:id] - Permite modificar una de sus notas al usuario registrado. TOKEN

- DELETE [/note/:id] - Borra una nota solo si es el usuario que la creo. TOKEN
