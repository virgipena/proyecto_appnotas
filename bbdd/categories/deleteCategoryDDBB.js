'use strict';

const getDB = require('../db');

const deleteCategoryDDBB = async (categoryId, userId) => {
  let connection;

  try {
    connection = await getDB();

    // Eliminamos la categoría.
    await connection.query(
      `
        DELETE FROM category 
        WHERE id = ?  AND user_id = ?
      `,
      [categoryId, userId]
    );
  } finally {
    if (connection) connection.release();
  }
};

module.exports = deleteCategoryDDBB;
