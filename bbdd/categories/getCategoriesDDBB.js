'use strict';
const getDB = require('../db');

const getCategoriesDDBB = async (userId) => {
  let connection;
  console.log(userId);
  try {
    // pido la conexion
    connection = await getDB();

    const [note] = await connection.query(
      `
        SELECT id, title 
        FROM category
        WHERE user_id = ?
      `,
      [userId]
    );

    return note;
  } finally {
    if (connection) connection.release();
  }
};

module.exports = getCategoriesDDBB;
