'use strict';
const getDB = require('../db');

const { generateError } = require('../../helpers');

const getCategoryDDBB = async (categoryId, userId) => {
  let connection;
  console.log(categoryId, userId);
  try {
    // pido la conexion
    connection = await getDB();

    const [category] = await connection.query(
      `
        SELECT user_id
        FROM category 
        WHERE id = ? AND user_id = ?
      `,
      [categoryId, userId]
    );

    // Si no existe la categoria lanzamos un error.
    if (category.length < 1) {
      generateError('Categoria no encontrada', 404);
    }

    // Si la categoria existe estara ubicada en la posicion 0 del array.
    // Retornamos esa posicion.
    return category[0];
  } finally {
    if (connection) connection.release();
  }
};

module.exports = getCategoryDDBB;
