'use strict';

const getDB = require('../db');

const { generateError } = require('../../helpers');

const insertCategoryDDBB = async (title, userId) => {
  let connection;

  try {
    connection = await getDB();

    if (title === '') {
      generateError('El título de la categoría no puede estar vacío', 400);
    }

    // Comprobamos si existe alguna categoria creada por el mismo usuario
    // con el mismo titulo.
    const [categorias] = await connection.query(
      `SELECT title FROM category WHERE title = ?`,
      [title]
    );

    // Si ya existe una categora creada por este usuaro con el mismo ttulo
    // lanzamos un error.
    if (categorias.length > 0) {
      generateError('Ya existe una categoría con ese titulo', 409);
    }

    // Insertamos la categoría.
    const [result] = await connection.query(
      `INSERT INTO category (title, user_id, created_at) VALUES (?, ?, ?)`,
      [title, userId, new Date()]
    );

    const { insertId } = result;

    return insertId;
  } finally {
    if (connection) connection.release();
  }
};

module.exports = insertCategoryDDBB;
