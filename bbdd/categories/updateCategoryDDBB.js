'use strict';

const getDB = require('../db');
const { generateError } = require('../../helpers');

const updateCategoryDDBB = async (title, idCategory, userId) => {
  let connection;

  try {
    connection = await getDB();

    if (title === '') {
      generateError('El título de la categoría no puede estar vacío', 400);
    }

    // Comprobamos si existe alguna categoria creada por el mismo usuario
    // con el mismo titulo.
    const [categorias] = await connection.query(
      `SELECT title FROM category WHERE title = ?`,
      [title]
    );

    // Si ya existe una categora creada por este usuaro con el mismo ttulo
    // lanzamos un error.
    if (categorias.length > 0) {
      generateError('Ya existe una categoría con ese titulo', 409);
    }

    // Actualizamos categoria.
    await connection.query(
      `
        UPDATE category 
        SET title = ?
        WHERE id = ? AND user_id = ?
      `,
      [title, idCategory, userId]
    );
  } finally {
    if (connection) connection.release();
  }
};

module.exports = updateCategoryDDBB;
