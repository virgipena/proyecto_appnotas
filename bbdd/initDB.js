'use strict';

// script que ejecutaremos, desde la root del proyecto (donde está el .env), con:
// node ./bbdd/initDB.js

require('dotenv').config();

const mysql = require('mysql2/promise');

const { MYSQL_HOST, MYSQL_USER, MYSQL_PASSWORD, MYSQL_DATABASE } = process.env;

// Variable que almacenará un grupo de conexiones.

let pool;

/* 
  Función que crea un pool de connecciones al DB (si no existe)
  y devuelve una connección 
*/
async function getBBDD() {
  if (!pool) {
    pool = mysql.createPool({
      connectionLimit: 10,
      host: MYSQL_HOST, //IP de mysql
      user: MYSQL_USER, //usuario mysql
      password: MYSQL_PASSWORD, // pwd usuario mysql
      timezone: 'Z',
    });
  }
  return await pool.getConnection();
}
const getDB = getBBDD();

let connection;

async function createDatabase() {
  try {
    connection = await getDB;
    await connection.query(`CREATE DATABASE IF NOT EXISTS ${MYSQL_DATABASE}`);

    await connection.query(`USE ${MYSQL_DATABASE}`);

    // Eliminamos las tablas.
    await connection.query(`DROP TABLE IF EXISTS note`);
    await connection.query(`DROP TABLE IF EXISTS category`);
    await connection.query(`DROP TABLE IF EXISTS user`);

    // CREO LAS TABLAS
    console.log('Creando tablas....');
    await connection.query(`
      CREATE TABLE user (
        id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
        email VARCHAR(100) UNIQUE NOT NULL,
        password VARCHAR(100) NOT NULL,
        created_at TIMESTAMP NOT NULL
      );
    `);

    await connection.query(`
      CREATE TABLE category (
        id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
        user_id INT UNSIGNED NOT NULL,
        FOREIGN KEY (user_id) REFERENCES user(id),
        title VARCHAR(100) NOT NULL,
        created_at TIMESTAMP NOT NULL
      )
    `);

    await connection.query(`
      CREATE TABLE note (
        id INT UNSIGNED PRIMARY KEY AUTO_INCREMENT,
        user_id INT UNSIGNED NOT NULL,
        FOREIGN KEY (user_id) REFERENCES user(id),
        category_id INT UNSIGNED,
        FOREIGN KEY (category_id) REFERENCES category(id) ON DELETE SET NULL,
        text VARCHAR(3000) NOT NULL,
        title VARCHAR(100) NOT NULL,
        image VARCHAR(100),
        is_public BOOLEAN NOT NULL DEFAULT false, 
        created_at TIMESTAMP NOT NULL
      )
    `);
  } catch (error) {
    console.error('ERROR:', error.message);
    process.exit(1);
  } finally {
    //Si hay conexión, la liberamos.
    if (connection) {
      connection.release();
    }

    //Cerramos el proceso.
    process.exit(0);
  }
}

createDatabase();
