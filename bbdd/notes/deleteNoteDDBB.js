'use strict';
const getDB = require('../db');

const deleteNoteDDBB = async (noteId, userId) => {
  let connection;

  try {
    // pido la conexion
    connection = await getDB();

    await connection.query(
      `
        DELETE
        FROM note
        WHERE id = ? AND user_id = ?`,
      [noteId, userId]
    );
  } finally {
    if (connection) connection.release();
  }
};

module.exports = deleteNoteDDBB;
