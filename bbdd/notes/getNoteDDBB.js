'use strict';
const getDB = require('../db');

const { generateError } = require('../../helpers');

const getNoteDDBB = async (noteId, userId) => {
  let connection;

  try {
    // pido la conexion
    connection = await getDB();

    const [note] = await connection.query(
      `
        SELECT 
          n.id, 
          n.user_id, 
          u.email, 
          n.category_id, 
          c.title AS categoryTitle, 
          n.text, n.title, 
          n.is_public, 
          n.image, 
          IFNULL (n.user_id = ?, 0) AS owner,
          n.created_at
        FROM note n
        JOIN user u ON n.user_id = u.id 
        LEFT JOIN category c ON n.category_id = c.id
        WHERE n.id = ?
      `,
      [userId, noteId]
    );

    // Si la nota no existe lanzamos un error.
    if (note.length === 0) {
      generateError('La nota que quiere visualizar no existe', 404);
    }

    // Si la nota existe estar en la posicion cero del array.
    return note[0];
  } finally {
    if (connection) connection.release();
  }
};

module.exports = getNoteDDBB;
