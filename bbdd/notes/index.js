const getNoteDDBB = require('./getNoteDDBB');
const insertNoteDDBB = require('./insertNoteDDBB');
const updateNoteDDBB = require('./updateNoteDDBB');
const deleteNoteDDBB = require('./deleteNoteDDBB');
const listNotesDDBB = require('./listNotesDDBB');
module.exports = {
  getNoteDDBB,
  insertNoteDDBB,
  updateNoteDDBB,
  deleteNoteDDBB,
  listNotesDDBB,
};
