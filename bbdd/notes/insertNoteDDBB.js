'use strict';
const getDB = require('../db');

const insertNoteDDBB = async ({
  title,
  text,
  idCategory,
  isPublic,
  photoName,
  idUser,
}) => {
  let connection;
  console.log(title, text, idCategory, isPublic, photoName, idUser);
  try {
    // pido la conexion
    connection = await getDB();

    isPublic = isPublic === '1' ? true : false;

    const [result] = await connection.query(
      `
      INSERT INTO note (user_id, category_id, text, title, is_public, image, created_at)
      VALUES (?,?,?,?,?,?, now())
  `,
      [idUser, idCategory, text, title, isPublic, photoName]
    );

    const { insertId } = result;

    return insertId;
  } finally {
    if (connection) connection.release();
  }
};

module.exports = insertNoteDDBB;
