'use strict';
const getDB = require('../db');

const listNotesDDBB = async (userId) => {
  let connection;

  try {
    // pido la conexion
    connection = await getDB();

    const [notes] = await connection.query(
      `
        SELECT n.id, n.user_id, n.category_id, c.title as categoryTitle , n.text, n.title, n.is_public, n.image, n.created_at
        FROM note n
        LEFT JOIN category c ON n.category_id = c.id
        WHERE n.user_id = ?
        ORDER BY n.created_at DESC
      `,
      [userId]
    );

    return notes;
  } finally {
    if (connection) connection.release();
  }
};

module.exports = listNotesDDBB;
