'use strict';
const getDB = require('../db');

const updateNoteDDBB = async ({
  text,
  title,
  categoryId,
  isPublic,
  imageName,
  noteId,
  userId,
}) => {
  let connection;

  try {
    // pido la conexion
    connection = await getDB();

    await connection.query(
      `
        UPDATE note SET 
          title = ?,
          text = ?,
          category_id = ?,
          is_public = ?, 
          image = ?
        WHERE id = ? AND user_id = ?
      `,
      [title, text, categoryId, isPublic, imageName, noteId, userId]
    );
  } finally {
    if (connection) connection.release();
  }
};

module.exports = updateNoteDDBB;
