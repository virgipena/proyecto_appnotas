'use strict';

const getDB = require('../db');
const { generateError } = require('../../helpers');

const getUserById = async (id) => {
  let connection;

  try {
    connection = await getDB();

    const [users] = await connection.query(
      `SELECT id, email, created_at FROM user WHERE id = ?`,
      [id]
    );

    // Si el array de usuarios está vacío lanzo un error.
    if (users.length < 1) {
      generateError('Usuario no encontrado', 404);
    }

    // Retornamos al usuario de la posición 0.
    return users[0];
  } finally {
    if (connection) connection.release();
  }
};

module.exports = getUserById;
