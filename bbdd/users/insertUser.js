'use strict';

const getDB = require('../db');
const bcrypt = require('bcrypt');

const { generateError } = require('../../helpers');

const insertUser = async (email, password) => {
  let connection;

  try {
    connection = await getDB();

    // Comprobamos si existe algún usuario con el email que del body.
    const [usuarios] = await connection.query(
      `SELECT id FROM user WHERE email = ?`,
      [email]
    );

    // Si el email ya está pillado lanzamos un error.
    if (usuarios.length > 0) {
      generateError('Ya existe un usuario con ese email', 409);
    }

    // Encriptamos la contraseña.
    const hashedPass = await bcrypt.hash(password, 10);

    // Insertamos el usuario.
    await connection.query(
      `INSERT INTO user (email, password, created_at) VALUES (?, ?, ?)`,
      [email, hashedPass, new Date()]
    );
  } finally {
    if (connection) connection.release();
  }
};

module.exports = insertUser;
