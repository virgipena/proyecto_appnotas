'use strict';

const insertCategoryDDBB = require('../../bbdd/categories/insertCategoryDDBB');
const { validate } = require('../../helpers');
const { categoryBodySchema } = require('../../schemas/index');

const createCategory = async (req, res, next) => {
  try {
    await validate(categoryBodySchema, req.body);
    // Obtenemos los campos del body.
    const { title } = req.body;

    console.log(req.userInfo.id);

    // Insertamos la categoria en la base de datos.
    const insertId = await insertCategoryDDBB(title, req.userInfo.id);

    res.send({
      status: 'ok',
      message: `Categoría "${title}" agregada`,
      data: {
        category: {
          id: insertId,
          title,
        },
      },
    });
  } catch (err) {
    next(err);
  }
};

module.exports = createCategory;
