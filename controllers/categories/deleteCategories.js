'use strict';

const getCategoryBBDD = require('../../bbdd/categories/getCategoryDDBB');
const deleteCategoryDDBB = require('../../bbdd/categories/deleteCategoryDDBB');

const { generateError } = require('../../helpers');

const deleteCategories = async (req, res, next) => {
  try {
    const { id } = req.params;

    // Comprobamos si existe la categoria.
    const category = await getCategoryBBDD(id, req.userInfo.id);

    // Si no somos los dueños de la categoria lanzamos un error.
    if (category.user_id !== req.userInfo.id) {
      generateError('No tienes suficientes permisos', 401);
    }

    // Borramos la categoria en la base de datos.
    await deleteCategoryDDBB(id, req.userInfo.id);

    res.send({
      status: 'ok',
      message: 'Categoria eliminada',
    });
  } catch (err) {
    next(err);
  }
};

module.exports = deleteCategories;
