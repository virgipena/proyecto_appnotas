'use strict';
const getCategoriesDDBB = require('../../bbdd/categories/getCategoriesDDBB');

const getCategories = async (req, res, next) => {
  try {
    const Categories = await getCategoriesDDBB(req.userInfo.id);

    res.send({
      status: 'ok',
      message: Categories,
    });
  } catch (error) {
    next(error);
  }
};

module.exports = getCategories;
