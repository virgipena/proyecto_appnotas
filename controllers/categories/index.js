const createCategory = require('./createCategory');
const modifyCategory = require('./modifyCategory');
const deleteCategories = require('./deleteCategories');
const getCategories = require('./getCategories');

module.exports = {
  createCategory,
  modifyCategory,
  deleteCategories,
  getCategories,
};
