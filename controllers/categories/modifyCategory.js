'use strict';

const updateCategoryDDBB = require('../../bbdd/categories/updateCategoryDDBB');
const getCategoryBBDD = require('../../bbdd/categories/getCategoryDDBB');

const { validate, generateError } = require('../../helpers');
const { categoryBodySchema } = require('../../schemas/index');

const modifyCategory = async (req, res, next) => {
  try {
    await validate(categoryBodySchema, req.body);

    const { id } = req.params;
    const { title } = req.body;

    // Comprobamos si existe la categoria.
    const category = await getCategoryBBDD(id, req.userInfo.id);

    // Si no somos los dueños de la categoria lanzamos un error.
    if (category.user_id !== req.userInfo.id) {
      generateError('No tienes suficientes permisos', 401);
    }

    // Actualizamos la categoria en la base de datos.
    await updateCategoryDDBB(title, id, req.userInfo.id);

    res.send({
      status: 'ok',
      message: 'Categoria actualizada',
    });
  } catch (err) {
    next(err);
  }
};

module.exports = modifyCategory;
