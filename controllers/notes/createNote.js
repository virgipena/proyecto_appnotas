'use strict';
const { savePhoto, validate } = require('../../helpers');
const { insertNoteDDBB } = require('../../bbdd/notes/index');
const { createNoteBodySchema } = require('../../schemas/index');

const createNote = async (req, res, next) => {
  try {
    await validate(createNoteBodySchema, req.body);

    const { title, text, idCategory, isPublic } = req.body;

    let photoName;

    if (req.files?.file) {
      photoName = await savePhoto(req.files.file);
    }

    // creo la nota en el DB
    await insertNoteDDBB({
      title,
      text,
      idCategory,
      isPublic,
      photoName,
      idUser: req.userInfo.id,
    });

    // Devuelvo respuesta satisfactoria si se crea la nota
    res.status(201).send({
      status: 'ok',
      message: `Nota creada`,
    });
  } catch (error) {
    next(error);
  }
};

module.exports = createNote;
