'use strict';

const { getNoteDDBB } = require('../../bbdd/notes/index');
const { deleteNoteDDBB } = require('../../bbdd/notes/index');
const { deletePhoto, generateError } = require('../../helpers');

const deleteNote = async (req, res, next) => {
  try {
    const { id } = req.params;

    // Comprobamos si existe la nota.
    const note = await getNoteDDBB(id);

    // Si no somos los dueños de la nota lanzamos un error.
    if (note.user_id !== req.userInfo.id) {
      generateError('No tienes suficientes permisos', 401);
    }

    // Si la nota tiene imagen la eliminamos de la carpeta de subida de archivos.
    if (note.image) {
      await deletePhoto(note.image);
    }

    // Eliminamos la nota de la base de datos.
    await deleteNoteDDBB(id, req.userInfo.id);

    res.send({
      status: 'ok',
      message: 'nota eliminada',
    });
  } catch (error) {
    next(error);
  }
};

module.exports = deleteNote;
