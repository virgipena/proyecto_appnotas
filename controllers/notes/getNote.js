'use strict';
const { getNoteDDBB } = require('../../bbdd/notes/index');
const { generateError } = require('../../helpers');
const FALSE = 0;

const getNote = async (req, res, next) => {
  try {
    const noteId = req.params.id;
    const note = await getNoteDDBB(noteId, req.userInfo?.id);

    console.log(req.userInfo);

    if (note.is_public === FALSE && note.user_id !== req.userInfo?.id) {
      generateError('No le es permitido ver la nota', 404);
    }

    res.send({
      status: 'ok',
      data: {
        note,
      },
    });
  } catch (error) {
    next(error);
  }
};

module.exports = getNote;
