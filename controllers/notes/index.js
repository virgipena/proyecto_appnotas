const createNote = require('./createNote');
const getNote = require('./getNote');
const patchNote = require('./patchNote');
const deleteNote = require('./deleteNote');
const listNotes = require('./listNotes');
module.exports = {
  createNote,
  getNote,
  patchNote,
  deleteNote,
  listNotes,
};
