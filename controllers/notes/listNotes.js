'use strict';
const { listNotesDDBB } = require('../../bbdd/notes/index');

const listNotes = async (req, res, next) => {
  try {
    const notes = await listNotesDDBB(req.userInfo.id);

    res.send({
      status: 'ok',
      message: notes,
    });
  } catch (error) {
    next(error);
  }
};

module.exports = listNotes;
