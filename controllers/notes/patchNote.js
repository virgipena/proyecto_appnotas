'use strict';
const { updateNoteDDBB, getNoteDDBB } = require('../../bbdd/notes/index');
const {
  generateError,
  deletePhoto,
  validate,
  savePhoto,
} = require('../../helpers');
const { updateNoteBodySchema } = require('../../schemas/index');

const patchNote = async (req, res, next) => {
  try {
    await validate(updateNoteBodySchema, req.body);

    const { id } = req.params;
    let { idCategory, text, title, isPublic } = req.body;

    const note = await getNoteDDBB(id);

    // Si no somos los dueños de la nota lanzamos un error.
    if (note.user_id !== req.userInfo.id) {
      generateError('No tienes suficientes permisos', 401);
    }

    // Variable donde almacenaremos el nombre de la foto.
    let imageName;

    // Si existe el objeto req.files quiere decir que el usuario
    // ha enviado una foto.
    if (req.files?.file) {
      // Compruebo si la nota ya tena una foto asignada.
      if (note.image) {
        // Si la tiene la borro.
        await deletePhoto(note.image);
      }
      // Guardamos la foto en la carpeta de subida de archivos.
      imageName = await savePhoto(req.files.file);
    }

    // Comprobamos una a una las propiedades que recibimos en el body. Si alguna
    // propiedad es undefined tomaremos por defecto el valor que haya en la base
    // de datos.
    text = text || note.text;
    title = title || note.title;
    imageName = imageName || note.image;

    if (isPublic) {
      isPublic = isPublic === '1' ? true : false;
    } else {
      isPublic = note.is_public;
    }
    await updateNoteDDBB({
      text,
      title,
      categoryId: idCategory,
      isPublic,
      imageName,
      noteId: id,
      userId: req.userInfo.id,
    });

    res.send({
      status: 'ok',
      message: 'nota editada satisfactoriamente',
      data: {
        note: {
          text,
          title,
          category_id: idCategory,
          is_public: isPublic,
          image: imageName,
        },
      },
    });
  } catch (error) {
    next(error);
  }
};

module.exports = patchNote;
