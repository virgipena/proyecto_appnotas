const insertUser = require('../../bbdd/users/insertUser');

const { generateError } = require('../../helpers');

const createUser = async (req, res, next) => {
  try {
    // Obtenemos los campos del body.
    const { email, password } = req.body;

    // Si faltan campos lanzamos un error.
    if (!email || !password) {
      generateError('Faltan campos', 400);
    }

    // Insertamos el usuario en la base de datos.
    await insertUser(email, password);

    res.send({
      status: 'ok',
      message: 'Usuario creado',
    });
  } catch (err) {
    next(err);
  }
};

module.exports = createUser;
