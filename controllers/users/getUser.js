const getUserById = require('../../bbdd/users/getUserById');
const getUser = async (req, res, next) => {
  try {
    // Obtenemos la información del usuario.
    const user = await getUserById(req.userInfo.id);
    res.send({
      status: 'ok',
      data: {
        user,
      },
    });
  } catch (err) {
    next(err);
  }
};
module.exports = getUser;
