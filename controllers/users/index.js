const createUser = require('./createUser');
const loginUser = require('./loginUser');
const getUser = require('./getUser');

module.exports = {
  createUser,
  loginUser,
  getUser,
};
