const getUserByEmail = require('../../bbdd/users/getUserByEmail');
const bcrypt = require('bcrypt');
const jwt = require('jsonwebtoken');

const { generateError } = require('../../helpers');

const loginUser = async (req, res, next) => {
  try {
    // Obtenemos los datos del body.
    const { email, password } = req.body;

    // Si faltan campos lanzamos un error.
    if (!email || !password) {
      generateError('Faltan campos', 400);
    }

    // Localizamos al usuario con el email del body.
    const user = await getUserByEmail(email);

    // Comprobamos si las contraseñas coinciden.
    const validPass = await bcrypt.compare(password, user.password);

    // Si la contraseña es incorrecta lanzamos un error.
    if (!validPass) {
      generateError('Contraseña incorrecta', 401);
    }

    // Objeto con información que queremos agregar al token.
    const tokenInfo = {
      id: user.id,
    };

    // Creamos el token.
    const token = jwt.sign(tokenInfo, process.env.JWT_SECRET, {
      expiresIn: '7d',
    });

    res.send({
      status: 'ok',
      message: 'Usuario logueado',
      data: {
        token,
      },
    });
  } catch (err) {
    next(err);
  }
};

module.exports = loginUser;
