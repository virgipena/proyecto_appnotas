'use strict';

const sharp = require('sharp');
const uuid = require('uuid');
const path = require('path');
const fs = require('fs/promises');

/**
 * ####################
 * ## Generate Error ##
 * ####################
 */

const generateError = (msg, code) => {
  const err = new Error(msg);
  err.httpStatus = code;
  throw err;
};

/* 
  Función que crea el directorio ficheros estaticos si no existe
*/
async function createStaticDir(staticDirPath) {
  try {
    await fs.access(staticDirPath);
  } catch (error) {
    await fs.mkdir(staticDirPath);
  }
}

/* 
  Función que guarda la foto en el disco 
*/
const savePhoto = async (dataPhoto) => {
  // creo la imagen con sharp a partir del buffer
  const img = sharp(dataPhoto.data);

  // genero un nombre unico para la image
  const photoNameUniq = `${uuid.v4()}_${dataPhoto.name}`;

  // guardo la imagen en el directorio de los ficheros estaticos
  await img.toFile(
    path.join(__dirname, process.env.UPLOADS_DIRECTORY, photoNameUniq)
  );

  return photoNameUniq;
};

/* 
  Función que borra un fichero en el directorio de uploads 
*/
async function deletePhoto(photo) {
  const photoPath = path.join(__dirname, process.env.UPLOADS_DIRECTORY, photo);

  try {
    // Comprobamos si el archivo existe con el mtodo access.
    await fs.access(photoPath);
  } catch (error) {
    // Si el archivo no existe el metodo access lanza un error, por lo que entramos en el catch.
    // Finalizamos la funcin.
    return;
  }

  // Si hemos llegado hasta aqui quiere decir que la foto existe. La eliminamos.
  await fs.unlink(photoPath);
}

/**
 * Funcion que permite validar atributos basado en reglas del schema
 *
 * @param {*} schema reglas de los parametros
 * @param {*} data informacion que se va a validar
 * @returns añade un atributo error a la respuesta si alguna de las validaciones no se cumple
 */
async function validate(schema, data) {
  try {
    await schema.validateAsync(data);
  } catch (error) {
    error.httpStatus = 400;
    throw error;
  }
}

module.exports = {
  generateError,
  createStaticDir,
  savePhoto,
  deletePhoto,
  validate,
};
