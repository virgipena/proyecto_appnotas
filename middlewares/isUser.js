'use strict';

const jwt = require('jsonwebtoken');

const { generateError } = require('../helpers');

/* 
  Middleware check JWT
*/
const isUser = async (req, res, next) => {
  try {
    const { authorization } = req.headers;

    // Compruebo que la petición tenga en el header el token
    if (!authorization) {
      generateError('Falta cabecera de autorización', 401);
    }

    let tokenInfo;
    try {
      // verifico token (si modificado o caducado lanza error)
      tokenInfo = jwt.verify(authorization, process.env.JWT_SECRET);
    } catch (error) {
      generateError('Token no valido', 401);
    }

    // añado a la req las informaciones del usuario que hace la petición (payload token)
    req.userInfo = tokenInfo;

    next();
  } catch (error) {
    next(error);
  }
};

module.exports = isUser;
