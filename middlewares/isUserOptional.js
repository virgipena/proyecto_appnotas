'use strict';

const jwt = require('jsonwebtoken');

const { generateError } = require('../helpers');

/* 
  Middleware check JWT
*/
const isUserOptional = async (req, res, next) => {
  try {
    const { authorization } = req.headers;

    // Si hay token creamos el objeto userInfo, de lo contrario pasamos el control al siguiente middleware
    // o funcion controladora.
    if (authorization && authorization !== 'null') {
      let tokenInfo;

      try {
        // verifico token (si modificado o caducado lanza error)
        tokenInfo = jwt.verify(authorization, process.env.JWT_SECRET);
      } catch (error) {
        generateError('Token no valido', 401);
      }

      req.userInfo = tokenInfo;
    }

    next();
  } catch (error) {
    next(error);
  }
};

module.exports = isUserOptional;
