'use strict';

const Joi = require('joi');

//Schema con las reglas necesarias para validar los atributos del body al actualizar una nota
const updateNoteBodySchema = Joi.object().keys({
  text: Joi.string().max(3000),
  title: Joi.string().max(100),
  idCategory: Joi.number(),
  isPublic: Joi.string(),
  file: Joi.string(),
});

//Schema con las reglas necesarias para validar los atributos del body al actualizar una nota
const createNoteBodySchema = Joi.object().keys({
  text: Joi.string().max(3000).required(),
  title: Joi.string().max(100).required(),
  idCategory: Joi.number(),
  isPublic: Joi.string(),
  file: Joi.string(),
});

const categoryBodySchema = Joi.object().keys({
  title: Joi.string().max(100).required(),
});
module.exports = {
  updateNoteBodySchema,
  createNoteBodySchema,
  categoryBodySchema,
};
