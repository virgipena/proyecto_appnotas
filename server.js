'use strict';

// importo y configuro dotenv para
// aceder a las variables de entorno definidas en .env
require('dotenv').config();

const express = require('express');
const morgan = require('morgan');
const cors = require('cors');
const path = require('path');
const fileUpload = require('express-fileupload');

const { createUser, loginUser, getUser } = require('./controllers/users');

const {
  createCategory,
  deleteCategories,
  modifyCategory,
  getCategories,
} = require('./controllers/categories');

const {
  createNote,
  getNote,
  patchNote,
  deleteNote,
  listNotes,
} = require('./controllers/notes');

const isUser = require('./middlewares/isUser');
const isUserOptional = require('./middlewares/isUserOptional');
const { createStaticDir } = require('./helpers');

const { PORT, UPLOADS_DIRECTORY } = process.env;
const staticDirPath = path.join(__dirname, UPLOADS_DIRECTORY);

// creo una instancia de express
const app = express();

//cors
app.use(cors());

//middleware para permitir el envío de json
app.use(express.json());

app.use(express.static(staticDirPath));

// Creo el directorio statico si no existe
createStaticDir(staticDirPath);

// middleware para parsear los ficheros en el body
app.use(fileUpload());

// añado middleware morgan para hacer un log de las peticiones que me llegan
app.use(morgan('dev'));

/**
 * ##########################
 * ## Middlewares Usuarios ##
 * ##########################
 */

// Crear un nuevo usuario.
app.post('/user', createUser);

// Loguear usuario
app.post('/user/login', loginUser);

// Obtener usuario
app.get('/user', isUser, getUser);

/**
 * ############################
 * ## Middlewares Categorias ##
 * ############################
 */

// Obtener lista de categorias
app.get('/category', isUser, getCategories);

// Crear categoria
app.post('/category', isUser, createCategory);

// Actualizar categoria
app.patch('/category/:id', isUser, modifyCategory);

// Eliminar categoria
app.delete('/category/:id', isUser, deleteCategories);

/**
 * #######################
 * ## Middlewares Notas ##
 * #######################
 */

// Crear una nota
app.post('/note', isUser, createNote);

// Obtener una nota
app.get('/note/:id', isUserOptional, getNote);

// Obtener lista de notas
app.get('/note', isUser, listNotes);

// Modificar, actualizar una nota
app.patch('/note/:id', isUser, patchNote);

// Eliminar una nota
app.delete('/note/:id', isUser, deleteNote);

// middleware de los errores
app.use((error, req, res, next) => {
  console.log(error);
  res.status(error.httpStatus || 500).send({
    status: 'error',
    message: error.message,
  });
});

// middleware 404, endpoint no encontrado
app.use((req, res) => {
  res.status(404).send({
    status: 'error',
    message: 'Not found',
  });
});

// pongo en escucha express
app.listen(PORT, () => {
  console.log(`Servidor en http://127.0.0.1:${PORT}`);
});
